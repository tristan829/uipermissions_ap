using UIPermissions;

namespace TestUIPermissions
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TM_Dep25_RespPerson_false()
        {
            UIResult uIResult = UIPermissions.UIPermissions.getPermissions(UserDepartmant.Dep25, UserRole.RespPerson, false);
            Assert.AreEqual(uIResult.menu.isSrcDomestic, false);
            Assert.AreEqual(uIResult.menu.isSrcSecure, false);
            Assert.AreEqual(uIResult.menu.isSrcLaundry, false);

            Assert.AreEqual(uIResult.menu.isCaseManagement, true);

            Assert.AreEqual(uIResult.menu.isRptEconomyIntelligence, false);
            Assert.AreEqual(uIResult.menu.isRptEconomyLaundry, false);
            Assert.AreEqual(uIResult.menu.isRptIncorruptionIntelligence, false);
            Assert.AreEqual(uIResult.menu.isRptLaundryIntelligence, false);
            Assert.AreEqual(uIResult.menu.isRptUnitsProcStatus, false);
            Assert.AreEqual(uIResult.menu.isRptIntelligenceSource, false);

            Assert.AreEqual(uIResult.menu.isSystemUsers, false);
            Assert.AreEqual(uIResult.menu.isSystemUnitRespPerson, false);
            Assert.AreEqual(uIResult.menu.isBasicCode, false);

            Assert.AreEqual(uIResult.caseManagementTable.isEdit, true);
            Assert.AreEqual(uIResult.caseManagementTable.isView, true);
            Assert.AreEqual(uIResult.caseManagementTable.isBtnAddNewCase, false);

            Assert.AreEqual(uIResult.caseManagementDetail.caseManager, PageDetail.view);
            Assert.AreEqual(uIResult.caseManagementDetail.respPerson, PageDetail.edit);
              

        }



        [TestMethod]
        public void TM_Dep31_RespPerson_false()
        {
            UIResult uIResult = UIPermissions.UIPermissions.getPermissions(UserDepartmant.Dep31, UserRole.RespPerson, false);
            Assert.AreEqual(uIResult.menu.isSrcDomestic, false);
            Assert.AreEqual(uIResult.menu.isSrcSecure, false);
            Assert.AreEqual(uIResult.menu.isSrcLaundry, false);

            Assert.AreEqual(uIResult.menu.isCaseManagement, true);

            Assert.AreEqual(uIResult.menu.isRptEconomyIntelligence, false);
            Assert.AreEqual(uIResult.menu.isRptEconomyLaundry, false);
            Assert.AreEqual(uIResult.menu.isRptIncorruptionIntelligence, false);
            Assert.AreEqual(uIResult.menu.isRptLaundryIntelligence, false);
            Assert.AreEqual(uIResult.menu.isRptUnitsProcStatus, false);
            Assert.AreEqual(uIResult.menu.isRptIntelligenceSource, false);

            Assert.AreEqual(uIResult.menu.isSystemUsers, false);
            Assert.AreEqual(uIResult.menu.isSystemUnitRespPerson, false);
            Assert.AreEqual(uIResult.menu.isBasicCode, false);

            Assert.AreEqual(uIResult.caseManagementTable.isEdit, true);
            Assert.AreEqual(uIResult.caseManagementTable.isView, true);
            Assert.AreEqual(uIResult.caseManagementTable.isBtnAddNewCase, false);

            Assert.AreEqual(uIResult.caseManagementDetail.caseManager, PageDetail.view);
            Assert.AreEqual(uIResult.caseManagementDetail.respPerson, PageDetail.edit);


        }
        [TestMethod]
        public void TM_Dep25_RespPerson_true()
        {
            UIResult uIResult = UIPermissions.UIPermissions.getPermissions(UserDepartmant.Dep25, UserRole.RespPerson, true);
            Assert.AreEqual(uIResult.menu.isSrcDomestic, true);
            Assert.AreEqual(uIResult.menu.isSrcSecure, false);

            Assert.AreEqual(uIResult.menu.isSrcLaundry, true);

            Assert.AreEqual(uIResult.menu.isCaseManagement, true);

            Assert.AreEqual(uIResult.menu.isRptEconomyIntelligence, true);
            Assert.AreEqual(uIResult.menu.isRptEconomyLaundry, true);

            Assert.AreEqual(uIResult.menu.isRptIncorruptionIntelligence, false);
            Assert.AreEqual(uIResult.menu.isRptLaundryIntelligence, false);
            Assert.AreEqual(uIResult.menu.isRptUnitsProcStatus, false);
            Assert.AreEqual(uIResult.menu.isRptIntelligenceSource, false);

            Assert.AreEqual(uIResult.menu.isSystemUsers, true);
            Assert.AreEqual(uIResult.menu.isSystemUnitRespPerson, true);
            Assert.AreEqual(uIResult.menu.isBasicCode, true);

            Assert.AreEqual(uIResult.caseManagementTable.isEdit, true);
            Assert.AreEqual(uIResult.caseManagementTable.isView, true);
            Assert.AreEqual(uIResult.caseManagementTable.isBtnAddNewCase, true);

            Assert.AreEqual(uIResult.caseManagementDetail.caseManager, PageDetail.edit);
            Assert.AreEqual(uIResult.caseManagementDetail.respPerson, PageDetail.edit);


        }
        [TestMethod]
        public void TM_Dep31_RespPerson_true()
        {
            UIResult uIResult = UIPermissions.UIPermissions.getPermissions(UserDepartmant.Dep31, UserRole.RespPerson, true);
            Assert.AreEqual(uIResult.menu.isSrcDomestic, true);
            Assert.AreEqual(uIResult.menu.isSrcSecure, true);
            Assert.AreEqual(uIResult.menu.isSrcLaundry, true);

            Assert.AreEqual(uIResult.menu.isCaseManagement, true);

            Assert.AreEqual(uIResult.menu.isRptEconomyIntelligence, false);
            Assert.AreEqual(uIResult.menu.isRptEconomyLaundry, false);
            Assert.AreEqual(uIResult.menu.isRptIncorruptionIntelligence, true);
            Assert.AreEqual(uIResult.menu.isRptLaundryIntelligence, true);
            Assert.AreEqual(uIResult.menu.isRptUnitsProcStatus, true);
            Assert.AreEqual(uIResult.menu.isRptIntelligenceSource, true);

            Assert.AreEqual(uIResult.menu.isSystemUsers, true);
            Assert.AreEqual(uIResult.menu.isSystemUnitRespPerson, true);
            Assert.AreEqual(uIResult.menu.isBasicCode, true);

            Assert.AreEqual(uIResult.caseManagementTable.isEdit, true);
            Assert.AreEqual(uIResult.caseManagementTable.isView, true);
            Assert.AreEqual(uIResult.caseManagementTable.isBtnAddNewCase, true);

            Assert.AreEqual(uIResult.caseManagementDetail.caseManager, PageDetail.edit);
            Assert.AreEqual(uIResult.caseManagementDetail.respPerson, PageDetail.edit);


        }
        [TestMethod]
        public void TM_Dep25_CaseManager_false()
        {
            UIResult uIResult = UIPermissions.UIPermissions.getPermissions(UserDepartmant.Dep25, UserRole.CaseManager, false);
            Assert.AreEqual(uIResult.menu.isSrcDomestic, true);
            Assert.AreEqual(uIResult.menu.isSrcSecure, false);
            Assert.AreEqual(uIResult.menu.isSrcLaundry, true);

            Assert.AreEqual(uIResult.menu.isCaseManagement, true);

            Assert.AreEqual(uIResult.menu.isRptEconomyIntelligence, true);
            Assert.AreEqual(uIResult.menu.isRptEconomyLaundry, true);
            Assert.AreEqual(uIResult.menu.isRptIncorruptionIntelligence, false);
            Assert.AreEqual(uIResult.menu.isRptLaundryIntelligence, false);
            Assert.AreEqual(uIResult.menu.isRptUnitsProcStatus, false);
            Assert.AreEqual(uIResult.menu.isRptIntelligenceSource, false);

            Assert.AreEqual(uIResult.menu.isSystemUsers, false);
            Assert.AreEqual(uIResult.menu.isSystemUnitRespPerson, false);
            Assert.AreEqual(uIResult.menu.isBasicCode, false);

            Assert.AreEqual(uIResult.caseManagementTable.isEdit, true);
            Assert.AreEqual(uIResult.caseManagementTable.isView, true);
            Assert.AreEqual(uIResult.caseManagementTable.isBtnAddNewCase, true);

            Assert.AreEqual(uIResult.caseManagementDetail.caseManager, PageDetail.edit);
            Assert.AreEqual(uIResult.caseManagementDetail.respPerson, PageDetail.edit);


        }
        [TestMethod]
        public void TM_Dep31_CaseManager_false()
        {
            UIResult uIResult = UIPermissions.UIPermissions.getPermissions(UserDepartmant.Dep31, UserRole.CaseManager, false);
            Assert.AreEqual(uIResult.menu.isSrcDomestic, true);
            Assert.AreEqual(uIResult.menu.isSrcSecure, true);
            Assert.AreEqual(uIResult.menu.isSrcLaundry, true);

            Assert.AreEqual(uIResult.menu.isCaseManagement, true);

            Assert.AreEqual(uIResult.menu.isRptEconomyIntelligence, false);
            Assert.AreEqual(uIResult.menu.isRptEconomyLaundry, false);

            Assert.AreEqual(uIResult.menu.isRptIncorruptionIntelligence, true);
            Assert.AreEqual(uIResult.menu.isRptLaundryIntelligence, true);
            Assert.AreEqual(uIResult.menu.isRptUnitsProcStatus, true);
            Assert.AreEqual(uIResult.menu.isRptIntelligenceSource, true);

            Assert.AreEqual(uIResult.menu.isSystemUsers, false);
            Assert.AreEqual(uIResult.menu.isSystemUnitRespPerson, false);
            Assert.AreEqual(uIResult.menu.isBasicCode, false);

            Assert.AreEqual(uIResult.caseManagementTable.isEdit, true);
            Assert.AreEqual(uIResult.caseManagementTable.isView, true);
            Assert.AreEqual(uIResult.caseManagementTable.isBtnAddNewCase, true);

            Assert.AreEqual(uIResult.caseManagementDetail.caseManager, PageDetail.edit);
            Assert.AreEqual(uIResult.caseManagementDetail.respPerson, PageDetail.edit);



        }
        [TestMethod]
        public void TM_Dep25_CaseManager_true()
        {
            UIResult uIResult = UIPermissions.UIPermissions.getPermissions(UserDepartmant.Dep25, UserRole.CaseManager, true);
            Assert.AreEqual(uIResult.menu.isSrcDomestic, true);
            Assert.AreEqual(uIResult.menu.isSrcSecure, false);
            Assert.AreEqual(uIResult.menu.isSrcLaundry, true);

            Assert.AreEqual(uIResult.menu.isCaseManagement, true);

            Assert.AreEqual(uIResult.menu.isRptEconomyIntelligence, true);
            Assert.AreEqual(uIResult.menu.isRptEconomyLaundry, true);
            Assert.AreEqual(uIResult.menu.isRptIncorruptionIntelligence, false);
            Assert.AreEqual(uIResult.menu.isRptLaundryIntelligence, false);
            Assert.AreEqual(uIResult.menu.isRptUnitsProcStatus, false);
            Assert.AreEqual(uIResult.menu.isRptIntelligenceSource, false);

            Assert.AreEqual(uIResult.menu.isSystemUsers, true);
            Assert.AreEqual(uIResult.menu.isSystemUnitRespPerson, true);
            Assert.AreEqual(uIResult.menu.isBasicCode, true);

            Assert.AreEqual(uIResult.caseManagementTable.isEdit, true);
            Assert.AreEqual(uIResult.caseManagementTable.isView, true);
            Assert.AreEqual(uIResult.caseManagementTable.isBtnAddNewCase, true);

            Assert.AreEqual(uIResult.caseManagementDetail.caseManager, PageDetail.edit);
            Assert.AreEqual(uIResult.caseManagementDetail.respPerson, PageDetail.edit);


        }
        [TestMethod]
        public void TM_Dep31_CaseManager_true()
        {
            UIResult uIResult = UIPermissions.UIPermissions.getPermissions(UserDepartmant.Dep31, UserRole.CaseManager, true);
            Assert.AreEqual(uIResult.menu.isSrcDomestic, true);
            Assert.AreEqual(uIResult.menu.isSrcSecure, true);
            Assert.AreEqual(uIResult.menu.isSrcLaundry, true);

            Assert.AreEqual(uIResult.menu.isCaseManagement, true);

            Assert.AreEqual(uIResult.menu.isRptEconomyIntelligence, false);
            Assert.AreEqual(uIResult.menu.isRptEconomyLaundry, false);
            Assert.AreEqual(uIResult.menu.isRptIncorruptionIntelligence, true);
            Assert.AreEqual(uIResult.menu.isRptLaundryIntelligence, true);
            Assert.AreEqual(uIResult.menu.isRptUnitsProcStatus, true);
            Assert.AreEqual(uIResult.menu.isRptIntelligenceSource, true);

            Assert.AreEqual(uIResult.menu.isSystemUsers, true);
            Assert.AreEqual(uIResult.menu.isSystemUnitRespPerson, true);
            Assert.AreEqual(uIResult.menu.isBasicCode, true);

            Assert.AreEqual(uIResult.caseManagementTable.isEdit, true);
            Assert.AreEqual(uIResult.caseManagementTable.isView, true);
            Assert.AreEqual(uIResult.caseManagementTable.isBtnAddNewCase, true);

            Assert.AreEqual(uIResult.caseManagementDetail.caseManager, PageDetail.edit);
            Assert.AreEqual(uIResult.caseManagementDetail.respPerson, PageDetail.edit);


        }
        [TestMethod]
        public void TM_Dep25_Supervisor_false()
        {
            UIResult uIResult = UIPermissions.UIPermissions.getPermissions(UserDepartmant.Dep25, UserRole.Supervisor, false);
            Assert.AreEqual(uIResult.menu.isSrcDomestic, false);
            Assert.AreEqual(uIResult.menu.isSrcSecure, false);
            Assert.AreEqual(uIResult.menu.isSrcLaundry, false);

            Assert.AreEqual(uIResult.menu.isCaseManagement, true);

            Assert.AreEqual(uIResult.menu.isRptEconomyIntelligence, true);
            Assert.AreEqual(uIResult.menu.isRptEconomyLaundry, true);
            Assert.AreEqual(uIResult.menu.isRptIncorruptionIntelligence, false);
            Assert.AreEqual(uIResult.menu.isRptLaundryIntelligence, false);
            Assert.AreEqual(uIResult.menu.isRptUnitsProcStatus, false);
            Assert.AreEqual(uIResult.menu.isRptIntelligenceSource, false);

            Assert.AreEqual(uIResult.menu.isSystemUsers, false);
            Assert.AreEqual(uIResult.menu.isSystemUnitRespPerson, false);
            Assert.AreEqual(uIResult.menu.isBasicCode, false);

            Assert.AreEqual(uIResult.caseManagementTable.isEdit, false);
            Assert.AreEqual(uIResult.caseManagementTable.isView, true);
            Assert.AreEqual(uIResult.caseManagementTable.isBtnAddNewCase, false);

            Assert.AreEqual(uIResult.caseManagementDetail.caseManager, PageDetail.view);
            Assert.AreEqual(uIResult.caseManagementDetail.respPerson, PageDetail.view);


        }
        [TestMethod]
        public void TM_Dep31_Supervisor_false()
        {
            UIResult uIResult = UIPermissions.UIPermissions.getPermissions(UserDepartmant.Dep31, UserRole.Supervisor, false);
            Assert.AreEqual(uIResult.menu.isSrcDomestic, false);
            Assert.AreEqual(uIResult.menu.isSrcSecure, false);
            Assert.AreEqual(uIResult.menu.isSrcLaundry, false);

            Assert.AreEqual(uIResult.menu.isCaseManagement, true);

            Assert.AreEqual(uIResult.menu.isRptEconomyIntelligence, false);
            Assert.AreEqual(uIResult.menu.isRptEconomyLaundry, false);
            Assert.AreEqual(uIResult.menu.isRptIncorruptionIntelligence, true);
            Assert.AreEqual(uIResult.menu.isRptLaundryIntelligence, true);
            Assert.AreEqual(uIResult.menu.isRptUnitsProcStatus, true);
            Assert.AreEqual(uIResult.menu.isRptIntelligenceSource, true);

            Assert.AreEqual(uIResult.menu.isSystemUsers, false);
            Assert.AreEqual(uIResult.menu.isSystemUnitRespPerson, false);
            Assert.AreEqual(uIResult.menu.isBasicCode, false);

            Assert.AreEqual(uIResult.caseManagementTable.isEdit, false);
            Assert.AreEqual(uIResult.caseManagementTable.isView, true);
            Assert.AreEqual(uIResult.caseManagementTable.isBtnAddNewCase, false);

            Assert.AreEqual(uIResult.caseManagementDetail.caseManager, PageDetail.view);
            Assert.AreEqual(uIResult.caseManagementDetail.respPerson, PageDetail.view);


        }
        [TestMethod]
        public void TM_Dep25_Supervisor_true()
        {
            UIResult uIResult = UIPermissions.UIPermissions.getPermissions(UserDepartmant.Dep25, UserRole.Supervisor, true);
            Assert.AreEqual(uIResult.menu.isSrcDomestic, true);
            Assert.AreEqual(uIResult.menu.isSrcSecure, false);
            Assert.AreEqual(uIResult.menu.isSrcLaundry, true);

            Assert.AreEqual(uIResult.menu.isCaseManagement, true);

            Assert.AreEqual(uIResult.menu.isRptEconomyIntelligence, true);
            Assert.AreEqual(uIResult.menu.isRptEconomyLaundry, true);
            Assert.AreEqual(uIResult.menu.isRptIncorruptionIntelligence, false);
            Assert.AreEqual(uIResult.menu.isRptLaundryIntelligence, false);
            Assert.AreEqual(uIResult.menu.isRptUnitsProcStatus, false);
            Assert.AreEqual(uIResult.menu.isRptIntelligenceSource, false);

            Assert.AreEqual(uIResult.menu.isSystemUsers, true);
            Assert.AreEqual(uIResult.menu.isSystemUnitRespPerson, true);
            Assert.AreEqual(uIResult.menu.isBasicCode, true);

            Assert.AreEqual(uIResult.caseManagementTable.isEdit, true);
            Assert.AreEqual(uIResult.caseManagementTable.isView, true);
            Assert.AreEqual(uIResult.caseManagementTable.isBtnAddNewCase, true);

            Assert.AreEqual(uIResult.caseManagementDetail.caseManager, PageDetail.edit);
            Assert.AreEqual(uIResult.caseManagementDetail.respPerson, PageDetail.edit);


        }
        [TestMethod]
        public void TM_Dep31_Supervisor_true()
        {
            UIResult uIResult = UIPermissions.UIPermissions.getPermissions(UserDepartmant.Dep31, UserRole.Supervisor, true);
            Assert.AreEqual(uIResult.menu.isSrcDomestic, true);
            Assert.AreEqual(uIResult.menu.isSrcSecure, true);
            Assert.AreEqual(uIResult.menu.isSrcLaundry, true);

            Assert.AreEqual(uIResult.menu.isCaseManagement, true);

            Assert.AreEqual(uIResult.menu.isRptEconomyIntelligence, false);
            Assert.AreEqual(uIResult.menu.isRptEconomyLaundry, false);
            Assert.AreEqual(uIResult.menu.isRptIncorruptionIntelligence, true);
            Assert.AreEqual(uIResult.menu.isRptLaundryIntelligence, true);
            Assert.AreEqual(uIResult.menu.isRptUnitsProcStatus, true);
            Assert.AreEqual(uIResult.menu.isRptIntelligenceSource, true);

            Assert.AreEqual(uIResult.menu.isSystemUsers, true);
            Assert.AreEqual(uIResult.menu.isSystemUnitRespPerson, true);
            Assert.AreEqual(uIResult.menu.isBasicCode, true);

            Assert.AreEqual(uIResult.caseManagementTable.isEdit, true);
            Assert.AreEqual(uIResult.caseManagementTable.isView, true);
            Assert.AreEqual(uIResult.caseManagementTable.isBtnAddNewCase, true);

            Assert.AreEqual(uIResult.caseManagementDetail.caseManager, PageDetail.edit);
            Assert.AreEqual(uIResult.caseManagementDetail.respPerson, PageDetail.edit);


        }


    }
}