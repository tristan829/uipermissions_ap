﻿using System.ComponentModel;

namespace UIPermissions
{
    public class UIPermissions
    {
        /// <summary>
        /// 使用者權限功能表判斷
        /// </summary>
        /// <param name="userDep"></param>
        /// <param name="userRole"></param>
        /// <param name="isAdmin"></param>
        /// <returns></returns>
        public static UIResult getPermissions(UserDepartmant userDep, UserRole userRole, bool isAdmin)
        {
            UIResult uIResult = new UIResult();
            //uIResult.menu 預設為 all false
            switch (userRole)
            {
                case UserRole.Supervisor:
                    //uIResult.menu.isSrcDomestic = true;
                    //uIResult.menu.isSrcSecure = true;
                    //uIResult.menu.isSrcLaundry = true;
                    uIResult.menu.isCaseManagement = true;
                    uIResult.menu.isRptEconomyIntelligence = true;
                    uIResult.menu.isRptEconomyLaundry = true;
                    uIResult.menu.isRptIncorruptionIntelligence = true;
                    uIResult.menu.isRptLaundryIntelligence = true;
                    uIResult.menu.isRptUnitsProcStatus = true;
                    uIResult.menu.isRptIntelligenceSource = true;
                    //uIResult.menu.isSystemUsers = false;
                    //uIResult.menu.isSystemUnitRespPerson = false;
                    //uIResult.menu.isBasicCode = false;

                    uIResult.caseManagementTable.isEdit = false;
                    uIResult.caseManagementTable.isView = true;
                    uIResult.caseManagementTable.isBtnAddNewCase = false;

                    uIResult.caseManagementDetail.caseManager = PageDetail.view;
                    uIResult.caseManagementDetail.respPerson = PageDetail.view;

                    break;
                case UserRole.CaseManager:
                    uIResult.menu.isSrcDomestic = true;
                    uIResult.menu.isSrcSecure = true;
                    uIResult.menu.isSrcLaundry = true;
                    uIResult.menu.isCaseManagement = true;
                    uIResult.menu.isRptEconomyIntelligence = true;
                    uIResult.menu.isRptEconomyLaundry = true;
                    uIResult.menu.isRptIncorruptionIntelligence = true;
                    uIResult.menu.isRptLaundryIntelligence = true;
                    uIResult.menu.isRptUnitsProcStatus = true;
                    uIResult.menu.isRptIntelligenceSource = true;
                    //uIResult.menu.isSystemUsers = true;
                    //uIResult.menu.isSystemUnitRespPerson = true;
                    //uIResult.menu.isBasicCode = true;

                    uIResult.caseManagementTable.isEdit = true;
                    uIResult.caseManagementTable.isView = true;
                    uIResult.caseManagementTable.isBtnAddNewCase = true;

                    uIResult.caseManagementDetail.caseManager = PageDetail.edit;
                    uIResult.caseManagementDetail.respPerson = PageDetail.edit;

                    break;
                case UserRole.RespPerson:
                    //uIResult.menu.isSrcDomestic = true;
                    //uIResult.menu.isSrcSecure = true;
                    //uIResult.menu.isSrcLaundry = true;
                    uIResult.menu.isCaseManagement = true;
                    //uIResult.menu.isRptEconomyIntelligence = true;
                    //uIResult.menu.isRptEconomyLaundry = true;
                    //uIResult.menu.isRptIncorruptionIntelligence = true;
                    //uIResult.menu.isRptLaundryIntelligence = true;
                    //uIResult.menu.isRptUnitsProcStatus = true;
                    //uIResult.menu.isRptIntelligenceSource = true;
                    //uIResult.menu.isSystemUsers = true;
                    //uIResult.menu.isSystemUnitRespPerson = true;
                    //uIResult.menu.isBasicCode = true;

                    uIResult.caseManagementTable.isEdit = true;
                    uIResult.caseManagementTable.isView = true;
                    uIResult.caseManagementTable.isBtnAddNewCase = false;

                    uIResult.caseManagementDetail.caseManager = PageDetail.view;
                    uIResult.caseManagementDetail.respPerson = PageDetail.edit;
                    break;
                default:
                    break;
            }

            if (isAdmin == true)
            {
                uIResult.menu.isSrcDomestic = true;
                uIResult.menu.isSrcSecure = true;
                uIResult.menu.isSrcLaundry = true;
                if(userDep== UserDepartmant.Dep25)
                    uIResult.menu.isSrcSecure = false;

                uIResult.menu.isRptEconomyIntelligence = true;
                uIResult.menu.isRptEconomyLaundry = true;
                uIResult.menu.isRptIncorruptionIntelligence = true;
                uIResult.menu.isRptLaundryIntelligence = true;
                uIResult.menu.isRptUnitsProcStatus = true;
                uIResult.menu.isRptIntelligenceSource = true;

                uIResult.menu.isSystemUsers = true;
                uIResult.menu.isSystemUnitRespPerson = true;
                uIResult.menu.isBasicCode = true;

                uIResult.caseManagementTable.isEdit = true;
                uIResult.caseManagementTable.isView = true;
                uIResult.caseManagementTable.isBtnAddNewCase = true;

                uIResult.caseManagementDetail.caseManager = PageDetail.edit;
                uIResult.caseManagementDetail.respPerson = PageDetail.edit;

                
            }
            else
            {
                uIResult.menu.isSystemUsers = false;
                uIResult.menu.isSystemUnitRespPerson = false;
                uIResult.menu.isBasicCode = false;
            }


            switch (userDep)
            {
                case UserDepartmant.Dep25:

                    //uIResult.menu.isRptEconomyIntelligence = true;
                    //uIResult.menu.isRptEconomyLaundry = true;
                    uIResult.menu.isRptIncorruptionIntelligence = false;
                    uIResult.menu.isRptLaundryIntelligence = false;
                    uIResult.menu.isRptUnitsProcStatus = false;
                    uIResult.menu.isRptIntelligenceSource = false;
                    break;
                case UserDepartmant.Dep31:
                    uIResult.menu.isRptEconomyIntelligence = false;
                    uIResult.menu.isRptEconomyLaundry = false;
                    //uIResult.menu.isRptIncorruptionIntelligence = true;
                    //uIResult.menu.isRptLaundryIntelligence = true;
                    //uIResult.menu.isRptUnitsProcStatus = true;
                    //uIResult.menu.isRptIntelligenceSource = true;
                    break;
                default:
                    break;
            }

            if (userDep == UserDepartmant.Dep25)
                uIResult.menu.isSrcSecure = false;

            return uIResult;
        }
    }

    public struct UIResult
    {
        /// <summary>
        /// 左列功能選單
        /// </summary>
        public Menu menu;
        /// <summary>
        /// 不法情資案件清單 - 表格功能
        /// </summary>
        public CaseManagementTable caseManagementTable;
        /// <summary>
        /// 不法情資案件內容 - 黃區/綠區
        /// </summary>
        public CaseManagementDetail caseManagementDetail;


    }
    public enum PageDetail
    {
        /// <summary>
        /// 可以編輯
        /// </summary>
        edit,
        /// <summary>
        /// 可以檢視
        /// </summary>
        view
    }
    public struct CaseManagementDetail
    {
        /// <summary>
        /// 可以編輯
        /// </summary>
        [Description("可以編輯")]
        public PageDetail caseManager;
        /// <summary>
        /// 可以檢視
        /// </summary>
        [Description("可以檢視")]
        public PageDetail respPerson;

        public CaseManagementDetail()
        {
            caseManager = PageDetail.view;
            respPerson = PageDetail.view;
        }
    }
    public struct CaseManagementTable
    {
        /// <summary>
        /// 可以編輯
        /// </summary>
        [Description("可以編輯")]
        public bool isEdit;
        /// <summary>
        /// 可以檢視
        /// </summary>
        [Description("可以檢視")]
        public bool isView;
        /// <summary>
        /// 可以新增案件
        /// </summary>
        [Description("可以新增案件")]
        public bool isBtnAddNewCase;
        public CaseManagementTable()
        {
            isEdit = false;
            isView = false;
            isBtnAddNewCase = false;
        }
    }
    public struct Menu
    {
        /// <summary>
        /// 國內安全調查處
        /// </summary>
        [Description("國內安全調查處")]
        public bool isSrcDomestic;
        /// <summary>
        /// 保防處
        /// </summary>
        [Description("保防處")]
        public bool isSrcSecure;
        /// <summary>
        /// 洗錢防制處
        /// </summary>
        [Description("洗錢防制處")]
        public bool isSrcLaundry;
        /// <summary>
        /// 不法情資案件管理
        /// </summary>
        [Description("不法情資案件管理")]
        public bool isCaseManagement;
        /// <summary>
        /// 經防處國情處理報表
        /// </summary>
        [Description("經防處國情處理報表")]
        public bool isRptEconomyIntelligence;
        /// <summary>
        /// 經防處洗錢處理報表
        /// </summary>
        [Description("經防處洗錢處理報表")]
        public bool isRptEconomyLaundry;
        /// <summary>
        /// 廉政處國情處理報表
        /// </summary>
        [Description("廉政處國情處理報表")]
        public bool isRptIncorruptionIntelligence;
        /// <summary>
        /// 廉政處洗錢處理報表
        /// </summary>
        [Description("廉政處洗錢處理報表")]
        public bool isRptLaundryIntelligence;
        /// <summary>
        /// 專案情資來源件數統計
        /// </summary>
        [Description("專案情資來源件數統計")]
        public bool isRptUnitsProcStatus;
        /// <summary>
        /// 專案情資來源件數統計
        /// </summary>
        [Description("專案情資來源件數統計")]
        public bool isRptIntelligenceSource;
        /// <summary>
        /// 使用者帳號管理頁面
        /// </summary>
        [Description("使用者帳號管理頁面")]
        public bool isSystemUsers;
        /// <summary>
        /// 單位承辦人頁面
        /// </summary>
        [Description("單位承辦人頁面")]
        public bool isSystemUnitRespPerson;
        /// <summary>
        /// 基礎代碼維護頁面
        /// </summary>
        [Description("基礎代碼維護頁面")]
        public bool isBasicCode;

        public Menu()
        {
            isSrcDomestic = false;
            isSrcSecure = false;
            isSrcLaundry = false;
            isCaseManagement = false;
            isRptEconomyIntelligence = false;
            isRptEconomyLaundry = false;
            isRptIncorruptionIntelligence = false;
            isRptLaundryIntelligence = false;
            isRptUnitsProcStatus = false;
            isRptIntelligenceSource = false;
            isSystemUsers = false;
            isSystemUnitRespPerson = false;
            isBasicCode = false;
        }
    }

    public enum UserDepartmant
    {
        /// <summary>
        /// 經防處
        /// </summary>
        [Description("經防處")]
        Dep25,
        /// <summary>
        /// 廉政處
        /// </summary>
        [Description("廉政處")]
        Dep31
    }
    public enum UserRole
    {
        /// <summary>
        /// 業管長官
        /// </summary>
        [Description("業管長官")]
        Supervisor,
        /// <summary>
        /// 分案人
        /// </summary>
        [Description("分案人")]
        CaseManager,
        /// <summary>
        /// 承辦人
        /// </summary>
        [Description("承辦人")]
        RespPerson
    }
















}